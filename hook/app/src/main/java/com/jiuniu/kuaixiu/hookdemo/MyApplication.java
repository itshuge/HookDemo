package com.jiuniu.kuaixiu.hookdemo;

import android.app.Application;

/**
 * Created by Administrator on 2018/11/27 0027.
 * DESC:TODO()
 */

public class MyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        HookUtil hookUtil = new HookUtil(ProxyActivity.class, this);
        hookUtil.hookSystemHandler();
        hookUtil.hookAms();
    }
}